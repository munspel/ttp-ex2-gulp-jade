﻿var gulp = require('gulp');
var plumber = require('gulp-plumber');
var pug = require('gulp-pug');
var copy = require('gulp-copy');
var browserSync = require('browser-sync').create();
var filenames = require("gulp-filenames");

function getCaruselImages() {
    var res = [];
    var flist = filenames.get("slides");
    for (var fname of flist) {
        res.push({image: "img/slides/" + fname});
    }
    return res;
}

function copyImages() {
    return gulp.src("./src/img/**")
        .pipe(gulp.dest("./dist/img/"))
}

function copyIcon() {
    return gulp.src("./src/*.ico")
        .pipe(gulp.dest("./dist/"))
}

function getSlides() {
    return gulp.src("./src/img/slides/*.*")
        .pipe(filenames("slides"))
}

function Pug() {
    return gulp.src('./src/*.pug')
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(pug({
            pretty: true,
            data: {
                title: 'Test project',
                slides: getCaruselImages()
            }
        }))
        .pipe(gulp.dest('./dist'))
}

function Server() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    browserSync.watch('dist', browserSync.reload)
}

function Watch() {
    gulp.watch('src/**/*.pug', gulp.series(Pug));
    gulp.watch('src/img/**', gulp.series(copyImages));
}

gulp.task('default', gulp.series(copyImages, copyIcon, getSlides, Pug, gulp.parallel(Server, Watch)));
